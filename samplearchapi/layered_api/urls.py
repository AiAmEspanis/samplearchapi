from django.urls import include, path
from layered_api.layered_api.urls import urlpatterns as api_urls


urlpatterns = [
    path('layered/', include(api_urls)),
]

from django.apps import AppConfig


class LayeredApiConfig(AppConfig):
    name = 'layered_api'

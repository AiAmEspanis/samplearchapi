from django.apps import AppConfig


class ModularApiConfig(AppConfig):
    name = 'modular_api'

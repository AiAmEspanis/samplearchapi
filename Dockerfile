
FROM python:3.6-slim

RUN apt-get update && \
	apt-get install -y --no-install-recommends \
    libpq-dev python-dev wget build-essential && \
    rm -rf /var/lib/apt/lists/*

RUN wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -P /tmp/
RUN chmod +x /tmp/wait-for-it.sh

RUN pip install pipenv
ENV PIPENV_PIPFILE /tmp/Pipfile
COPY Pipfile /tmp/Pipfile

COPY ./samplearchapi/ /usr/src/app
WORKDIR /usr/src/app

RUN pipenv lock && pipenv sync
